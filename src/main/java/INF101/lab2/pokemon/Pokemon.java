package INF101.lab2.pokemon;

import java.lang.Math;
import java.util.Random;

public class Pokemon {
    ////// Oppgave 1a
    // Create field variables here:
    String name;
    int healthPoints;
    int maxHealthPoints;
    int strength;
    Random random = new Random();
    ///// Oppgave 1b
    // Create a constructor here:
    public Pokemon(String name, int healthPoints, int strength) {
        this.name = name;
        this.random = new Random();
        this.healthPoints = (int) Math.abs(Math.round(100 + 10 * random.nextGaussian()));
        this.maxHealthPoints = this.healthPoints;
        this.strength = (int) Math.abs(Math.round(20 + 10 * random.nextGaussian()));
    }

    ///// Oppgave 2
	/**
     * Get name of the pokémon
     * @return name of pokémon
     */
    String getName() {
        return this.name;
    }

    /**
     * Get strength of the pokémon
     * @return strength of pokémon
     */
    int getStrength() {
        return this.strength;

    }

    /**
     * Get current health points of pokémon
     * @return current HP of pokémon
     */
    int getCurrentHP() {
        return this.healthPoints;
    }
    
    /**
     * Get maximum health points of pokémon
     * @return max HP of pokémon
     */
    int getMaxHP() {
        return maxHealthPoints;
    }

    /**
     * Check if the pokémon is alive. 
     * A pokemon is alive if current HP is higher than 0
     * @return true if current HP > 0, false if not
     */
    boolean isAlive() {
        if (healthPoints > 0) {
            return true;
        } 
        else {
            return false;
        }
    }


    
    ///// Oppgave 4
    /**
     * Damage the pokémon. This method reduces the number of
     * health points the pokémon has by <code>damageTaken</code>.
     * If <code>damageTaken</code> is higher than the number of current
     * health points then set current HP to 0.
     *
     * It should not be possible to deal negative damage, i.e. increase the number of health points.
     *
     * The method should print how much HP the pokemon is left with.
     *
     * @param damageTaken
     */
    void damage(int damageTaken) {
        if (damageTaken < 0) {
            damageTaken = 0;
        }
        healthPoints -= damageTaken;
        if (healthPoints < 0) {
            healthPoints = 0;
        }
        System.out.println(this.name+" takes "+damageTaken+" damage and is left with "+healthPoints+"/"+maxHealthPoints+" HP");
    }

    ///// Oppgave 5
    /**
     * Attack another pokémon. The method conducts an attack by <code>this</code>
     * on <code>target</code>. Calculate the damage using the pokémons strength
     * and a random element. Reduce <code>target</code>s health.
     * 
     * If <code>target</code> has 0 HP then print that it was defeated.
     * 
     * @param target pokémon that is being attacked
     */
    void attack(Pokemon target) {
        int damageInflicted = (int) (this.strength + this.strength / 2 * random.nextGaussian());
        System.out.println(this.name + " attacks " + target.getName()+".");
        target.damage(damageInflicted); 
        if (target.getCurrentHP() == 0) {
            System.out.println(target.getName() + " is defeated by "+this.name+".");
        }
    }

    ///// Oppgave 3
    @Override
    public String toString() {
        return this.name + " HP:" + " (" + healthPoints + "/" + maxHealthPoints + ") STR: " + strength;
    }

}
